package ru.tsc.kirillov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.TaskChangeStatusByIndexRequest;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.event.ConsoleEvent;
import ru.tsc.kirillov.tm.util.TerminalUtil;

@Component
public final class TaskCompletedByIndexListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String getName() {
        return "task-completed-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Завершить задачу по её индексу.";
    }

    @Override
    @EventListener(condition = "@taskCompletedByIndexListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[Завершение задачи по индексу]");
        System.out.println("Введите индекс задачи:");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        @NotNull final TaskChangeStatusByIndexRequest request =
                new TaskChangeStatusByIndexRequest(getToken(), index, Status.COMPLETED);
        getTaskEndpoint().changeTaskStatusByIndex(request);
    }

}
