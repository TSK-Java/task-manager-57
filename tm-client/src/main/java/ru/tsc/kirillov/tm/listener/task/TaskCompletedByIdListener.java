package ru.tsc.kirillov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.TaskChangeStatusByIdRequest;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.event.ConsoleEvent;
import ru.tsc.kirillov.tm.util.TerminalUtil;

@Component
public final class TaskCompletedByIdListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String getName() {
        return "task-completed-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Завершить задачу по её ID.";
    }

    @Override
    @EventListener(condition = "@taskCompletedByIdListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("Завершение задачи по ID]");
        System.out.println("Введите ID задачи:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskChangeStatusByIdRequest request =
                new TaskChangeStatusByIdRequest(getToken(), id, Status.COMPLETED);
        getTaskEndpoint().changeTaskStatusById(request);
    }

}
