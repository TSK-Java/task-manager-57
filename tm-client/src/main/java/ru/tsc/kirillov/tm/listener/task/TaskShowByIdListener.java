package ru.tsc.kirillov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.TaskGetByIdRequest;
import ru.tsc.kirillov.tm.dto.response.TaskGetByIdResponse;
import ru.tsc.kirillov.tm.event.ConsoleEvent;
import ru.tsc.kirillov.tm.util.TerminalUtil;

@Component
public final class TaskShowByIdListener extends AbstractTaskShowListener {

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Отобразить задачу по её ID.";
    }

    @Override
    @EventListener(condition = "@taskShowByIdListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[Отображение задачи по ID]");
        System.out.println("Введите ID задачи:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskGetByIdRequest request = new TaskGetByIdRequest(getToken(), id);
        @NotNull final TaskGetByIdResponse response = getTaskEndpoint().getTaskById(request);
        showTask(response.getTask());
    }

}
