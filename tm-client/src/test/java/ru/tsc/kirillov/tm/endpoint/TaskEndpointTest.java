package ru.tsc.kirillov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.kirillov.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.kirillov.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.kirillov.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.kirillov.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.kirillov.tm.api.service.IPropertyService;
import ru.tsc.kirillov.tm.dto.model.ProjectDTO;
import ru.tsc.kirillov.tm.dto.model.TaskDTO;
import ru.tsc.kirillov.tm.dto.request.*;
import ru.tsc.kirillov.tm.dto.response.*;
import ru.tsc.kirillov.tm.enumerated.Sort;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.marker.ISoapCategory;
import ru.tsc.kirillov.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Category(ISoapCategory.class)
public final class TaskEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(propertyService);

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @NotNull
    private final IProjectTaskEndpoint projectTaskEndpoint = IProjectTaskEndpoint.newInstance(propertyService);

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @Nullable
    private String tokenTest;

    @NotNull
    private final String invalid = UUID.randomUUID().toString();

    @NotNull
    private final String taskName = UUID.randomUUID().toString();

    @NotNull
    private final String taskDescription = UUID.randomUUID().toString();

    @NotNull
    private final String projectName = UUID.randomUUID().toString();

    @NotNull
    private final String projectDescription = UUID.randomUUID().toString();

    @NotNull final Date dateBegin = new Date();

    @NotNull final Date dateEnd = new Date();

    @Before
    public void initialization() {
        @NotNull final UserLoginResponse response = authEndpoint.login(
                new UserLoginRequest("test", "test")
        );
        tokenTest = response.getToken();
        taskEndpoint.clearTask(new TaskClearRequest(tokenTest));
        projectTaskEndpoint.clearProject(new ProjectClearRequest(tokenTest));
    }

    @After
    public void finalization() {
        taskEndpoint.clearTask(new TaskClearRequest(tokenTest));
        projectTaskEndpoint.clearProject(new ProjectClearRequest(tokenTest));
        tokenTest = null;
    }

    @Test
    public void createTask() {
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.createTask(
                        new TaskCreateRequest()
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.createTask(
                        new TaskCreateRequest("", "", "", null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.createTask(
                        new TaskCreateRequest(null, "", "", null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.createTask(
                        new TaskCreateRequest(tokenTest, "", "", null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.createTask(
                        new TaskCreateRequest(invalid, "", "", null, null)
                )
        );
        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(
                        new TaskCreateRequest(tokenTest, taskName, taskDescription, dateBegin, dateEnd)
        );
        Assert.assertNotNull(response);
        @Nullable TaskDTO task = response.getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(taskName, task.getName());
        Assert.assertEquals(taskDescription, task.getDescription());
        Assert.assertEquals(dateBegin.toString(), task.getDateBegin().toString());
        Assert.assertEquals(dateEnd.toString(), task.getDateEnd().toString());
    }

    @Test
    public void changeTaskStatusById() {
        @NotNull final TaskCreateResponse responseTask = taskEndpoint.createTask(
                new TaskCreateRequest(tokenTest, taskName, taskDescription, dateBegin, dateEnd)
        );
        Assert.assertNotNull(responseTask);
        @Nullable TaskDTO task = responseTask.getTask();
        Assert.assertNotNull(task);
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusById(
                        new TaskChangeStatusByIdRequest()
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusById(
                        new TaskChangeStatusByIdRequest(null, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusById(
                        new TaskChangeStatusByIdRequest("", null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusById(
                        new TaskChangeStatusByIdRequest(invalid, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusById(
                        new TaskChangeStatusByIdRequest(tokenTest, invalid, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusById(
                        new TaskChangeStatusByIdRequest(tokenTest, task.getId(), null)
                )
        );
        TaskChangeStatusByIdResponse response = taskEndpoint.changeTaskStatusById(
                        new TaskChangeStatusByIdRequest(tokenTest, task.getId(), Status.COMPLETED)
        );
        Assert.assertNotNull(response);
        @Nullable TaskDTO taskChanged = response.getTask();
        Assert.assertNotNull(taskChanged);
        Assert.assertNotEquals(task.getStatus(), Status.COMPLETED);
        Assert.assertNotEquals(task.getStatus(), taskChanged.getStatus());
        Assert.assertEquals(taskChanged.getStatus(), Status.COMPLETED);
    }

    @Test
    public void changeTaskStatusByIndex() {
        @NotNull final TaskCreateResponse responseTask = taskEndpoint.createTask(
                new TaskCreateRequest(tokenTest, taskName, taskDescription, dateBegin, dateEnd)
        );
        Assert.assertNotNull(responseTask);
        @Nullable TaskDTO task = responseTask.getTask();
        Assert.assertNotNull(task);
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusByIndex(
                        new TaskChangeStatusByIndexRequest()
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusByIndex(
                        new TaskChangeStatusByIndexRequest(null, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusByIndex(
                        new TaskChangeStatusByIndexRequest("", null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusByIndex(
                        new TaskChangeStatusByIndexRequest(invalid, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusByIndex(
                        new TaskChangeStatusByIndexRequest(tokenTest, -50, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusByIndex(
                        new TaskChangeStatusByIndexRequest(tokenTest, 50, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusByIndex(
                        new TaskChangeStatusByIndexRequest(tokenTest, 1, null)
                )
        );
        TaskChangeStatusByIndexResponse response = taskEndpoint.changeTaskStatusByIndex(
                new TaskChangeStatusByIndexRequest(tokenTest, 1, Status.COMPLETED)
        );
        Assert.assertNotNull(response);
        @Nullable TaskDTO taskChanged = response.getTask();
        Assert.assertNotNull(taskChanged);
        Assert.assertNotEquals(task.getStatus(), Status.COMPLETED);
        Assert.assertNotEquals(task.getStatus(), taskChanged.getStatus());
        Assert.assertEquals(taskChanged.getStatus(), Status.COMPLETED);
    }

    @Test
    public void listTask() {
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.listTask(new TaskListRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.listTask(new TaskListRequest(null, null))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.listTask(new TaskListRequest("", null))
        );
        @NotNull final TaskListResponse responseEmptyList =
                taskEndpoint.listTask(new TaskListRequest(tokenTest));
        Assert.assertNotNull(responseEmptyList);
        @Nullable List<TaskDTO> tasksEmpty = responseEmptyList.getTasks();
        Assert.assertNull(tasksEmpty);
        int countTask = 10;
        @Nullable final List<String> taskIdList = new ArrayList<>();
        for (int i = 0; i < countTask; i++) {
            @NotNull final String taskName = String.format("Task_%d", i);
            @NotNull final TaskCreateResponse responseTask = taskEndpoint.createTask(
                    new TaskCreateRequest(tokenTest, taskName, taskDescription, dateBegin, dateEnd)
            );
            Assert.assertNotNull(responseTask);
            @Nullable TaskDTO task = responseTask.getTask();
            Assert.assertNotNull(task);
            taskIdList.add(task.getId());
        }

        @NotNull TaskListResponse response =
                taskEndpoint.listTask(new TaskListRequest(tokenTest));
        Assert.assertNotNull(response);
        @Nullable List<TaskDTO> tasks = response.getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(countTask, tasks.size());
        @NotNull List<String> taskIdListCopy = new ArrayList<>(taskIdList);
        for(@NotNull final TaskDTO task : tasks) {
            @NotNull final String taskId = task.getId();
            Assert.assertTrue(taskIdListCopy.contains(taskId));
            taskIdListCopy.remove(taskId);
        }
        Assert.assertEquals(0, taskIdListCopy.size());

        response = taskEndpoint.listTask(new TaskListRequest(tokenTest, Sort.BY_NAME));
        Assert.assertNotNull(response);
        tasks = response.getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(countTask, tasks.size());
        taskIdListCopy = new ArrayList<>(taskIdList);
        int idx = 0;
        for(@NotNull final TaskDTO task : tasks) {
            @NotNull final String taskId = task.getId();
            Assert.assertTrue(taskIdListCopy.contains(taskId));
            taskIdListCopy.remove(taskId);
            Assert.assertEquals(String.format("Task_%d", idx++), task.getName());
        }
        Assert.assertEquals(0, taskIdListCopy.size());
    }

    @Test
    public void getTaskById() {
        @NotNull final TaskCreateResponse responseTask = taskEndpoint.createTask(
                new TaskCreateRequest(tokenTest, taskName, taskDescription, dateBegin, dateEnd)
        );
        Assert.assertNotNull(responseTask);
        @Nullable TaskDTO task = responseTask.getTask();
        Assert.assertNotNull(task);
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.getTaskById(
                        new TaskGetByIdRequest()
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.getTaskById(
                        new TaskGetByIdRequest(null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.getTaskById(
                        new TaskGetByIdRequest("", null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.getTaskById(
                        new TaskGetByIdRequest(invalid, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.getTaskById(
                        new TaskGetByIdRequest(tokenTest, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.getTaskById(
                        new TaskGetByIdRequest(tokenTest, "")
                )
        );
        @Nullable TaskGetByIdResponse responseNull = taskEndpoint.getTaskById(
                new TaskGetByIdRequest(tokenTest, invalid)
        );
        Assert.assertNotNull(responseNull);
        Assert.assertNull(responseNull.getTask());
        @Nullable TaskGetByIdResponse response = taskEndpoint.getTaskById(
                new TaskGetByIdRequest(tokenTest, task.getId())
        );
        Assert.assertNotNull(response);
        @Nullable TaskDTO taskGet = response.getTask();
        Assert.assertNotNull(taskGet);
        Assert.assertEquals(task.getId(), taskGet.getId());
        Assert.assertEquals(task.getName(), taskGet.getName());
        Assert.assertEquals(task.getDescription(), taskGet.getDescription());
        Assert.assertEquals(task.getDateBegin().toString(), taskGet.getDateBegin().toString());
        Assert.assertEquals(task.getDateEnd().toString(), taskGet.getDateEnd().toString());
    }

    @Test
    public void getTaskByIndex() {
        @NotNull final TaskCreateResponse responseTask = taskEndpoint.createTask(
                new TaskCreateRequest(tokenTest, taskName, taskDescription, dateBegin, dateEnd)
        );
        Assert.assertNotNull(responseTask);
        @Nullable TaskDTO task = responseTask.getTask();
        Assert.assertNotNull(task);
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.getTaskByIndex(
                        new TaskGetByIndexRequest()
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.getTaskByIndex(
                        new TaskGetByIndexRequest(null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.getTaskByIndex(
                        new TaskGetByIndexRequest("", null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.getTaskByIndex(
                        new TaskGetByIndexRequest(invalid, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.getTaskByIndex(
                        new TaskGetByIndexRequest(tokenTest, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.getTaskByIndex(
                        new TaskGetByIndexRequest(tokenTest, -50)
                )
        );
        @Nullable TaskGetByIndexResponse response = taskEndpoint.getTaskByIndex(
                new TaskGetByIndexRequest(tokenTest, 1)
        );
        Assert.assertNotNull(response);
        @Nullable TaskDTO taskGet = response.getTask();
        Assert.assertNotNull(taskGet);
        Assert.assertEquals(task.getId(), taskGet.getId());
        Assert.assertEquals(task.getName(), taskGet.getName());
        Assert.assertEquals(task.getDescription(), taskGet.getDescription());
        Assert.assertEquals(task.getDateBegin().toString(), taskGet.getDateBegin().toString());
        Assert.assertEquals(task.getDateEnd().toString(), taskGet.getDateEnd().toString());
    }

    @Test
    public void updateTaskById() {
        @NotNull final TaskCreateResponse responseTask = taskEndpoint.createTask(
                new TaskCreateRequest(tokenTest, taskName, taskDescription, dateBegin, dateEnd)
        );
        Assert.assertNotNull(responseTask);
        @Nullable TaskDTO task = responseTask.getTask();
        Assert.assertNotNull(task);
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(
                        new TaskUpdateByIdRequest()
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(
                        new TaskUpdateByIdRequest(null, null, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(
                        new TaskUpdateByIdRequest("", null, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(
                        new TaskUpdateByIdRequest(invalid, null, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(
                        new TaskUpdateByIdRequest(tokenTest, null, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(
                        new TaskUpdateByIdRequest(tokenTest, "", null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(
                        new TaskUpdateByIdRequest(tokenTest, task.getId(), "", null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(
                        new TaskUpdateByIdRequest(tokenTest, task.getId(), invalid, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(
                        new TaskUpdateByIdRequest(tokenTest, task.getId(), invalid, "")
                )
        );
        @NotNull String newName = UUID.randomUUID().toString();
        @NotNull String newDescription = UUID.randomUUID().toString();
        @Nullable TaskUpdateByIdResponse response = taskEndpoint.updateTaskById(
                new TaskUpdateByIdRequest(tokenTest, task.getId(), newName, newDescription)
        );
        Assert.assertNotNull(response);
        @Nullable TaskDTO taskUpdate = response.getTask();
        Assert.assertNotNull(taskUpdate);
        Assert.assertEquals(task.getId(), taskUpdate.getId());
        Assert.assertEquals(newName, taskUpdate.getName());
        Assert.assertEquals(newDescription, taskUpdate.getDescription());
        Assert.assertEquals(task.getDateBegin().toString(), taskUpdate.getDateBegin().toString());
        Assert.assertEquals(task.getDateEnd().toString(), taskUpdate.getDateEnd().toString());
    }

    @Test
    public void updateTaskByIndex() {
        @NotNull final TaskCreateResponse responseTask = taskEndpoint.createTask(
                new TaskCreateRequest(tokenTest, taskName, taskDescription, dateBegin, dateEnd)
        );
        Assert.assertNotNull(responseTask);
        @Nullable TaskDTO task = responseTask.getTask();
        Assert.assertNotNull(task);
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskByIndex(
                        new TaskUpdateByIndexRequest()
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskByIndex(
                        new TaskUpdateByIndexRequest(null, null, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskByIndex(
                        new TaskUpdateByIndexRequest("", null, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskByIndex(
                        new TaskUpdateByIndexRequest(invalid, null, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskByIndex(
                        new TaskUpdateByIndexRequest(tokenTest, null, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskByIndex(
                        new TaskUpdateByIndexRequest(tokenTest, -50, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskByIndex(
                        new TaskUpdateByIndexRequest(tokenTest, 50, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskByIndex(
                        new TaskUpdateByIndexRequest(tokenTest, 1, "", null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskByIndex(
                        new TaskUpdateByIndexRequest(tokenTest, 1, invalid, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskByIndex(
                        new TaskUpdateByIndexRequest(tokenTest, 1, invalid, "")
                )
        );
        @NotNull String newName = UUID.randomUUID().toString();
        @NotNull String newDescription = UUID.randomUUID().toString();
        @Nullable TaskUpdateByIndexResponse response = taskEndpoint.updateTaskByIndex(
                new TaskUpdateByIndexRequest(tokenTest, 1, newName, newDescription)
        );
        Assert.assertNotNull(response);
        @Nullable TaskDTO taskUpdate = response.getTask();
        Assert.assertNotNull(taskUpdate);
        Assert.assertEquals(task.getId(), taskUpdate.getId());
        Assert.assertEquals(newName, taskUpdate.getName());
        Assert.assertEquals(newDescription, taskUpdate.getDescription());
        Assert.assertEquals(task.getDateBegin().toString(), taskUpdate.getDateBegin().toString());
        Assert.assertEquals(task.getDateEnd().toString(), taskUpdate.getDateEnd().toString());
    }

    @Test
    public void clearTask() {
        @NotNull final TaskListResponse responseEmptyList =
                taskEndpoint.listTask(new TaskListRequest(tokenTest));
        Assert.assertNotNull(responseEmptyList);
        @Nullable List<TaskDTO> tasksEmpty = responseEmptyList.getTasks();
        Assert.assertNull(tasksEmpty);
        @NotNull final TaskCreateResponse responseTask = taskEndpoint.createTask(
                new TaskCreateRequest(tokenTest, taskName, taskDescription, dateBegin, dateEnd)
        );
        Assert.assertNotNull(responseTask);
        @Nullable TaskDTO task = responseTask.getTask();
        Assert.assertNotNull(task);
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.clearTask(
                        new TaskClearRequest()
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.clearTask(
                        new TaskClearRequest(null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.clearTask(
                        new TaskClearRequest("")
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.clearTask(
                        new TaskClearRequest(invalid)
                )
        );
        @NotNull TaskListResponse response = taskEndpoint.listTask(new TaskListRequest(tokenTest));
        Assert.assertNotNull(response);
        @Nullable List<TaskDTO> tasks = response.getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
        taskEndpoint.clearTask(new TaskClearRequest(tokenTest));
        response = taskEndpoint.listTask(new TaskListRequest(tokenTest));
        Assert.assertNotNull(response);
        Assert.assertNull(response.getTasks());
    }

    @Test
    public void listTaskByProjectId() {
        @NotNull final TaskCreateResponse responseTask = taskEndpoint.createTask(
                new TaskCreateRequest(tokenTest, taskName, taskDescription, dateBegin, dateEnd)
        );
        taskEndpoint.createTask(
                new TaskCreateRequest(tokenTest, taskName, taskDescription, dateBegin, dateEnd)
        );
        @Nullable TaskDTO task = responseTask.getTask();
        Assert.assertNotNull(task);
        @NotNull final ProjectCreateResponse responseProject = projectEndpoint.createProject(
                new ProjectCreateRequest(tokenTest, projectName, projectDescription, dateBegin, dateEnd)
        );
        @Nullable ProjectDTO project = responseProject.getProject();
        Assert.assertNotNull(project);
        projectTaskEndpoint.bindTaskToProjectId(new ProjectBindTaskByIdRequest(tokenTest, project.getId(), task.getId()));
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.listTaskByProjectId(
                        new TaskListByProjectIdRequest()
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.listTaskByProjectId(
                        new TaskListByProjectIdRequest(null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.listTaskByProjectId(
                        new TaskListByProjectIdRequest("", null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.listTaskByProjectId(
                        new TaskListByProjectIdRequest(invalid, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.listTaskByProjectId(
                        new TaskListByProjectIdRequest(tokenTest, "")
                )
        );
        @Nullable TaskListByProjectIdResponse response = taskEndpoint.listTaskByProjectId(
            new TaskListByProjectIdRequest(tokenTest, invalid)
        );
        Assert.assertNotNull(response);
        @Nullable List<TaskDTO> tasks = response.getTasks();
        Assert.assertNull(tasks);
        response = taskEndpoint.listTaskByProjectId(
                new TaskListByProjectIdRequest(tokenTest, project.getId())
        );
        Assert.assertNotNull(response);
        tasks = response.getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
        @Nullable final TaskDTO taskByProject = tasks.get(0);
        Assert.assertNotNull(taskByProject);
        Assert.assertEquals(task.getId(), taskByProject.getId());
    }

    @Test
    public void removeTaskById() {
        @NotNull final TaskCreateResponse responseTask = taskEndpoint.createTask(
                new TaskCreateRequest(tokenTest, taskName, taskDescription, dateBegin, dateEnd)
        );
        Assert.assertNotNull(responseTask);
        @Nullable TaskDTO task = responseTask.getTask();
        Assert.assertNotNull(task);
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.removeTaskById(
                        new TaskRemoveByIdRequest()
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.removeTaskById(
                        new TaskRemoveByIdRequest(null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.removeTaskById(
                        new TaskRemoveByIdRequest("", null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.removeTaskById(
                        new TaskRemoveByIdRequest(invalid, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.removeTaskById(
                        new TaskRemoveByIdRequest(tokenTest, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.removeTaskById(
                        new TaskRemoveByIdRequest(tokenTest, "")
                )
        );
        @NotNull TaskListResponse responseList = taskEndpoint.listTask(new TaskListRequest(tokenTest));
        Assert.assertNotNull(responseList);
        @Nullable List<TaskDTO> tasks = responseList.getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
        @Nullable TaskRemoveByIdResponse response = taskEndpoint.removeTaskById(
                new TaskRemoveByIdRequest(tokenTest, task.getId())
        );
        Assert.assertNotNull(response);
        responseList = taskEndpoint.listTask(new TaskListRequest(tokenTest));
        Assert.assertNotNull(responseList);
        tasks = responseList.getTasks();
        Assert.assertNull(tasks);
    }

    @Test
    public void removeTaskByIndex() {
        @NotNull final TaskCreateResponse responseTask = taskEndpoint.createTask(
                new TaskCreateRequest(tokenTest, taskName, taskDescription, dateBegin, dateEnd)
        );
        Assert.assertNotNull(responseTask);
        @Nullable TaskDTO task = responseTask.getTask();
        Assert.assertNotNull(task);
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.removeTaskByIndex(
                        new TaskRemoveByIndexRequest()
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.removeTaskByIndex(
                        new TaskRemoveByIndexRequest(null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.removeTaskByIndex(
                        new TaskRemoveByIndexRequest("", null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.removeTaskByIndex(
                        new TaskRemoveByIndexRequest(invalid, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.removeTaskByIndex(
                        new TaskRemoveByIndexRequest(tokenTest, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.removeTaskByIndex(
                        new TaskRemoveByIndexRequest(tokenTest, -50)
                )
        );
        @NotNull TaskListResponse responseList = taskEndpoint.listTask(new TaskListRequest(tokenTest));
        Assert.assertNotNull(responseList);
        @Nullable List<TaskDTO> tasks = responseList.getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
        @Nullable TaskRemoveByIndexResponse response = taskEndpoint.removeTaskByIndex(
                new TaskRemoveByIndexRequest(tokenTest, 1)
        );
        Assert.assertNotNull(response);
        responseList = taskEndpoint.listTask(new TaskListRequest(tokenTest));
        Assert.assertNotNull(responseList);
        tasks = responseList.getTasks();
        Assert.assertNull(tasks);
    }

}
