package ru.tsc.kirillov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.kirillov.tm.api.repository.model.IUserRepository;
import ru.tsc.kirillov.tm.model.User;

import javax.persistence.TypedQuery;
import java.util.Optional;

@Repository
@Scope("prototype")
public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository() {
        super(User.class);
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        @NotNull final String jpql = String.format("FROM %s m WHERE m.login = :login", getModelName());
        @NotNull final TypedQuery<User> query = entityManager.createQuery(jpql, clazz)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("login", login);
        return getResult(query);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        @NotNull final String jpql = String.format("FROM %s m WHERE m.email = :email", getModelName());
        @NotNull final TypedQuery<User> query = entityManager.createQuery(jpql, clazz)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("email", email);
        return getResult(query);
    }

    @Nullable
    @Override
    public User removeByLogin(@NotNull final String login) {
        @NotNull final Optional<User> user = Optional.ofNullable(findByLogin(login));
        user.ifPresent(this::remove);
        return user.orElse(null);
    }

}
