package ru.tsc.kirillov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.model.ProjectDTO;

public interface IProjectServiceDTO extends IUserOwnedServiceDTO<ProjectDTO> {

    @NotNull
    String[] findAllId(@Nullable final String userId);

}
