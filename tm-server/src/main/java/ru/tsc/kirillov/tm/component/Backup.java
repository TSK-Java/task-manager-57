package ru.tsc.kirillov.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.api.service.IDomainService;
import ru.tsc.kirillov.tm.api.service.ILoggerService;
import ru.tsc.kirillov.tm.api.service.IPropertyService;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Getter
@Component
public final class Backup {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IDomainService domainService;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    public void start() {
        if (!getPropertyService().getBackupEnabled()) return;
        load();
        es.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    public void save() {
        getDomainService().saveDataBackup();
    }

    public void load() {
        try {
            getDomainService().loadDataBackup();
        } catch (Exception e) {
            getLoggerService().info("Невозможно загрузить состояние приложение из бекапа: " + e.getMessage());
        }
    }

}
