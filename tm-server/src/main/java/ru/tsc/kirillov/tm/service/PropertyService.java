package ru.tsc.kirillov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.tsc.kirillov.tm.api.service.IPropertyService;

import java.util.Properties;

@Service
public class PropertyService implements IPropertyService {

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "12345";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "4484ds48er7y8r97y8rt7y8";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    private static final String SERVER_HOST_DEFAULT = "localhost";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private static final String SERVER_PORT_DEFAULT = "8080";

    @NotNull
    private static final String SESSION_KEY = "session.key";

    @NotNull
    private static final String SESSION_KEY_DEFAULT = "sdf7fsdge";

    @NotNull
    private static final String SESSION_TIMEOUT = "session.timeout";

    @NotNull
    private static final String SESSION_TIMEOUT_DEFAULT = "86400";

    @NotNull
    private static final String DATABASE_USER_NAME_KEY = "database.username";

    @NotNull
    private static final String DATABASE_USER_NAME_DEFAULT = "root";

    @NotNull
    private static final String DATABASE_USER_PASSWORD_KEY = "database.password";

    @NotNull
    private static final String DATABASE_USER_PASSWORD_DEFAULT = "root";

    @NotNull
    private static final String DATABASE_URL_KEY = "database.url";

    @NotNull
    private static final String DATABASE_URL_DEFAULT = "jdbc:mysql://localhost:3306/tm";

    @NotNull
    private static final String DATABASE_DRIVER_KEY = "database.driver";

    @NotNull
    private static final String DATABASE_DRIVER_DEFAULT = "com.mysql.jdbc.Driver";

    @NotNull
    private static final String DATABASE_DIALECT_KEY = "database.dialect";

    @NotNull
    private static final String DATABASE_DIALECT_DEFAULT = "org.hibernate.dialect.MySQL5InnoDBDialect";

    @NotNull
    private static final String DATABASE_HBM2DDL_AUTO_KEY = "database.hbm2ddl_auto";

    @NotNull
    private static final String DATABASE_HBM2DDL_AUTO_DEFAULT = "update";

    @NotNull
    private static final String DATABASE_SHOW_SQL_KEY = "database.show_sql";

    @NotNull
    private static final String DATABASE_SHOW_SQL_DEFAULT = "false";

    @NotNull
    private static final String DATABASE_FORMAT_SQL_KEY = "database.format_sql";

    @NotNull
    private static final String DATABASE_FORMAT_SQL_DEFAULT = "false";

    @NotNull
    private static final String DATABASE_USE_L2_CACHE_KEY = "database.cache.use_second_level_cache";

    @NotNull
    private static final String DATABASE_USE_L2_CACHE_DEFAULT = "false";

    @NotNull
    private static final String DATABASE_PROVIDER_CONFIG_FILE_RESOURCE_PATH_KEY =
            "database.cache.provider_configuration_file_resource_path";

    @NotNull
    private static final String DATABASE_PROVIDER_CONFIG_FILE_RESOURCE_PATH_DEFAULT = "hazelcast.xml";

    @NotNull
    private static final String DATABASE_REGION_FACTORY_CLASS_KEY = "database.cache.region.factory_class";

    @NotNull
    private static final String DATABASE_REGION_FACTORY_CLASS_DEFAULT =
            "com.hazelcast.hibernate.HazelcastCacheRegionFactory";

    @NotNull
    private static final String DATABASE_USE_QUERY_CACHE_KEY = "database.cache.user_query_cache";

    @NotNull
    private static final String DATABASE_USE_QUERY_CACHE_DEFAULT = "true";

    @NotNull
    private static final String DATABASE_USE_MINIMAL_PUTS_KEY = "database.cache.use_minimal_puts";

    @NotNull
    private static final String DATABASE_USE_MINIMAL_PUTS_DEFAULT = "true";

    @NotNull
    private static final String DATABASE_USE_LITE_MEMBER_KEY = "database.cache.hazelcast.use_lite_member";

    @NotNull
    private static final String DATABASE_USE_LITE_MEMBER_DEFAULT = "true";

    @NotNull
    private static final String DATABASE_REGION_PREFIX_KEY = "database.cache.region_prefix";

    @NotNull
    private static final String DATABASE_REGION_PREFIX_DEFAULT = "task-manager";

    @NotNull
    private static final String BACKUP_ENABLED_KEY = "backup.enabled";

    @NotNull
    private static final String BACKUP_ENABLED_DEFAULT = "false";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace('.', '_').toUpperCase();
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private Integer getIntegerValue(@NotNull final String key, @NotNull final String defaultValue) {
        @NotNull String value = getStringValue(key, defaultValue);
        return Integer.parseInt(value);
    }

    private boolean getBooleanValue(@NotNull final String key, @NotNull final String defaultValue) {
        @NotNull String value = getStringValue(key, defaultValue);
        return Boolean.parseBoolean(value);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @Override
    @NotNull
    public String getServerHost() {
        return getStringValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        return getIntegerValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getIntegerValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public String getSessionKey() {
        return getStringValue(SESSION_KEY, SESSION_KEY_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getSessionTimeout() {
        return getIntegerValue(SESSION_TIMEOUT, SESSION_TIMEOUT_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseUserName() {
        return getStringValue(DATABASE_USER_NAME_KEY, DATABASE_USER_NAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseUserPassword() {
        return getStringValue(DATABASE_USER_PASSWORD_KEY, DATABASE_USER_PASSWORD_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseUrl() {
        return getStringValue(DATABASE_URL_KEY, DATABASE_URL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseDriver() {
        return getStringValue(DATABASE_DRIVER_KEY, DATABASE_DRIVER_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseDialect() {
        return getStringValue(DATABASE_DIALECT_KEY, DATABASE_DIALECT_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseHbm2ddlAuto() {
        return getStringValue(DATABASE_HBM2DDL_AUTO_KEY, DATABASE_HBM2DDL_AUTO_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseShowSql() {
        return getStringValue(DATABASE_SHOW_SQL_KEY, DATABASE_SHOW_SQL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseFormatSql() {
        return getStringValue(DATABASE_FORMAT_SQL_KEY, DATABASE_FORMAT_SQL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseUseL2Cache() {
        return getStringValue(DATABASE_USE_L2_CACHE_KEY, DATABASE_USE_L2_CACHE_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseProviderConfigFileResourcePath() {
        return getStringValue(
                DATABASE_PROVIDER_CONFIG_FILE_RESOURCE_PATH_KEY,
                DATABASE_PROVIDER_CONFIG_FILE_RESOURCE_PATH_DEFAULT
        );
    }

    @NotNull
    @Override
    public String getDatabaseRegionFactoryClass() {
        return getStringValue(DATABASE_REGION_FACTORY_CLASS_KEY, DATABASE_REGION_FACTORY_CLASS_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseUserQueryCache() {
        return getStringValue(DATABASE_USE_QUERY_CACHE_KEY, DATABASE_USE_QUERY_CACHE_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseUseMinimalPuts() {
        return getStringValue(DATABASE_USE_MINIMAL_PUTS_KEY, DATABASE_USE_MINIMAL_PUTS_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseUseLiteMember() {
        return getStringValue(DATABASE_USE_LITE_MEMBER_KEY, DATABASE_USE_LITE_MEMBER_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseRegionPrefix() {
        return getStringValue(DATABASE_REGION_PREFIX_KEY, DATABASE_REGION_PREFIX_DEFAULT);
    }

    @Override
    public boolean getBackupEnabled() {
        return getBooleanValue(BACKUP_ENABLED_KEY, BACKUP_ENABLED_DEFAULT);
    }
}
