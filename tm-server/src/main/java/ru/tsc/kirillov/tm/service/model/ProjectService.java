package ru.tsc.kirillov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.tsc.kirillov.tm.api.repository.model.IProjectRepository;
import ru.tsc.kirillov.tm.api.service.model.IProjectService;
import ru.tsc.kirillov.tm.model.Project;

@Service
public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    @NotNull
    @Override
    protected IProjectRepository getRepository() {
        return context.getBean(IProjectRepository.class);
    }

}
