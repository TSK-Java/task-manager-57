package ru.tsc.kirillov.tm.exception.entity;

public final class TaskNotFoundException extends AbstractEntityNotFoundException {

    public TaskNotFoundException() {
        super("Ошибка! Задача не найдена.");
    }

}
